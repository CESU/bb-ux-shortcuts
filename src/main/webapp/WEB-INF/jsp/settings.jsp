<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<%@ page import="blackboard.platform.plugin.*" %>
<%@ page import="blackboard.platform.intl.*" %>

<%@ page import="java.util.*" %>

<%@ taglib uri="/bbNG" prefix="bbNG" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<fmt:setLocale value="${language}" />
<fmt:setBundle basename="text" />

<!DOCTYPE html>
<fmt:message key="settings.pageHeader.title" var="pageTitle"/>
<bbNG:genericPage title="${ pageTitle }" ctxId="ctx">

	<fmt:message key="settings.pageHeader.instructions" var="headerInstructions"/>
	<bbNG:pageHeader instructions="${ headerInstructions }">
		<bbNG:breadcrumbBar environment="PORTAL" navItem="course_plugin_manage">
			<fmt:message key="settings.breadcrumb" var="breadcrumb"/>
			<bbNG:breadcrumb title="${ breadcrumb }" />
		</bbNG:breadcrumbBar>
		<fmt:message key="settings.pageHeader.title" var="headerTitle"/>
		<bbNG:pageTitleBar showTitleBar="true" title="${ headerTitle }" />
		<div align='right'>
			<a href='http://cesu.au.dk/' target='_blank'>
				<span style="color:#ffffff;"><fmt:message key="general.meta.credits"/></span>
			</a>
		</div>
	</bbNG:pageHeader>
	
	<center>
		<table style="background-color:whitesmoke;border-collapse:collapse;width:800px;border: 1px solid gray;">
		  <tr>
		    <td style="width:100px;font-family:'Times New Roman',Times,serif;text-align:center;font-size:700%;font-weight:bold;font-style:italic;">i</td>
		    <td>
		    	<br />
		    	<br />
		    	<p><fmt:message key="general.meta.feedback.text"/></p><br />
		    	<p>
		    		<span><fmt:message key="general.meta.feedback.email"/></span>
		    		<span><a href="https://medarbejdere.au.dk/administration/studieadministration/studiesystemer/blackboard/blackboardsupport/" target="_top">www.medarbejdere.au.dk/administration/studieadministration/studiesystemer/blackboard/blackboardsupport/</a></span>
		    	</p>
		    	<br />
		    	<p>
		    		<fmt:message key="general.meta.feedback.moreInfo"/><br />
		    		<a href="http://cesu.au.dk/" target="_blank"><fmt:message key="general.meta.feedback.website"/></a>
	    		</p>
		    	<br />
		    	<br />
		    </td>
		  </tr>
		</table>
	</center>
	
	<bbNG:form 
		action="settings" 
		method="POST" 
		onsubmit="return validateForm()">
		
		<bbNG:dataCollection showSubmitButtons="true">
			
			<fmt:message key="edit.displayStep.title" var="displayStepTitle"/>
			<fmt:message key="settings.displayStep.instructions" var="displayStepInstructions"/>
			<bbNG:step title="${ displayStepTitle }" instructions="${ displayStepInstructions }">
			
				<bbNG:settingsPageList className="models.Shortcut" collection="${scList}" objectVar="sc" >
				
					<fmt:message key="edit.displayStep.tableHeadings.caption" var="tableHeadingCaption"/>
					<bbNG:listElement label="${ tableHeadingCaption }" name="sc" isRowHeader="true">
						<c:choose>
							<c:when test="${ language == 'da' }">
								${sc.captionDa}
							</c:when>
							
							<c:when test="${ language == 'en' }">
								${sc.captionEn}
							</c:when> 
						</c:choose>
					</bbNG:listElement>
					
					<fmt:message key="edit.displayStep.tableHeadings.display" var="tableHeadingDisplay"/>
					<bbNG:listElement label="${ tableHeadingDisplay }" name="isDisplayed" >
						<bbNG:dataElement>
							<bbNG:checkboxElement id="dis_lcbe${ sc.getId().getExternalString() }" name="dis_lcbe${ sc.getId().getExternalString() }" isSelected="${ sc.isDisplayed() }" value="${ sc.isDisplayed() }" onchange="value = checked; if(value === 'false'){req_lcbe${ sc.getId().getExternalString() }.checked = false; req_lcbe${ sc.getId().getExternalString() }.value = false;}"/>
						</bbNG:dataElement>
					</bbNG:listElement>
					
					<fmt:message key="edit.displayStep.tableHeadings.require" var="tableHeadingRequire"/>
					<bbNG:listElement label="${ tableHeadingRequire }" name="isRequired" >
						<bbNG:dataElement>
							<bbNG:checkboxElement id="req_lcbe${ sc.getId().getExternalString() }" name="req_lcbe${ sc.getId().getExternalString() }" isSelected="${ sc.isRequired() }" value="${ sc.isRequired() }" onchange="value = checked; if(value === 'true'){dis_lcbe${ sc.getId().getExternalString() }.checked = true; dis_lcbe${ sc.getId().getExternalString() }.value = true;}"/>
						</bbNG:dataElement>
					</bbNG:listElement>
				</bbNG:settingsPageList>
				
			</bbNG:step>
			
			<bbNG:stepSubmit cancelUrl="${ recallUrl }" showCancelButton="true">
				<fmt:message key="edit.form.submit" var="formSubmit"/>
				<bbNG:stepSubmitButton label="${ formSubmit }" />
			</bbNG:stepSubmit>
		</bbNG:dataCollection>
	</bbNG:form>	
</bbNG:genericPage>