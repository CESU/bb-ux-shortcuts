<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="/bbNG" prefix="bbNG"%>
<%@ taglib uri="/bbUI" prefix="bbUI"%>

<bbNG:includedPage>

	<bbNG:cssFile href="uxsc.css"/>
	<bbNG:cssFile href="https://use.fontawesome.com/releases/v5.3.1/css/all.css"/>
		
	<ul class="sc-list">
		<c:forEach items="${scList}" var="sc">
			<li>
				<div class="sc" onClick="location.href='${sc.linkUrl}';">
					<i class="fas fa-2x">&#x${sc.imageSource};</i>

					<span>
						<c:choose>
							<c:when test="${ language == 'da' }">
								${sc.captionDa}
							</c:when>
							<c:when test="${ language == 'en' }">
								${sc.captionEn}
							</c:when>
						</c:choose>
					</span>
				</div>

				<%-- <a href="${sc.linkUrl}">

						<i class="fas fa-fw">&#x${sc.imageSource};</i>

						<span>
							<c:choose>
								<c:when test="${ language == 'da' }">
									${sc.captionDa}
								</c:when>
								<c:when test="${ language == 'en' }">
									${sc.captionEn}
								</c:when>
							</c:choose>
						</span>

				</a>--%>
			</li>
		</c:forEach>
	</ul>
</bbNG:includedPage>