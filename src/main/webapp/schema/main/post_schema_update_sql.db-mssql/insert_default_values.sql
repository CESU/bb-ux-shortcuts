INSERT INTO cesu_uxsc_sc (linkurl, imagesource, caption_en, caption_da, display_ind, required_ind)
	SELECT x.linkurl, x.imagesource, x.caption_en, x.caption_da, x.display_ind, x.required_ind
		FROM (
			SELECT 
				'/webapps/portal/execute/tabs/tabAction?tab_tab_group_id=_1_1&forwardUrl=edit_module%2F_3_1%2Fbbcourseorg%3Fcmd%3Dedit&recallUrl=%2Fwebapps%2Fportal%2Fexecute%2Ftabs%2FtabAction%3Ftab_tab_group_id%3D_1_1' as linkurl, 
				'f03a' as imagesource,
				'My Courses Settings' as caption_en,
				'Indstillinger for Mine Kurser' as caption_da,
				'Y' as display_ind,
				'Y' as required_ind
			UNION
			SELECT
				'/webapps/blackboard/execute/personalSettings?command=edit', 
				'f1ab',
				'Language Settings',
				'Sprog Indstillinger',
				'Y',
				'N'
			UNION
			SELECT
				'/webapps/blackboard/execute/nautilus/notificationSettingsCaret?action=display', 
				'f0f3',
				'Notifications Settings',
				'Indstillinger for Notifikationer',
				'Y',
				'Y'
		) as x
	WHERE
		(SELECT COUNT(*) FROM cesu_uxsc_sc) <= 0;
