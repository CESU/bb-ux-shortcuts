DECLARE cnt NUMBER;

BEGIN
  SELECT COUNT(*) INTO cnt FROM cesu_uxsc_sc;
  IF cnt <= 0 THEN
      INSERT INTO cesu_uxsc_sc(pk1, linkUrl, imageSource, caption_en, caption_da, display_ind, required_ind) VALUES (cesu_uxsc_sc_SEQ.nextval, '/webapps/portal/execute/tabs/tabAction?tab_tab_group_id=_1_1&forwardUrl=edit_module%2F_3_1%2Fbbcourseorg%3Fcmd%3Dedit&recallUrl=%2Fwebapps%2Fportal%2Fexecute%2Ftabs%2FtabAction%3Ftab_tab_group_id%3D_1_1', 'f03a', 'My Courses Settings', 'Indstillinger for Mine Kurser', 'Y', 'Y');
      INSERT INTO cesu_uxsc_sc(pk1, linkUrl, imageSource, caption_en, caption_da, display_ind, required_ind) VALUES (cesu_uxsc_sc_SEQ.nextval, '/webapps/blackboard/execute/personalSettings?command=edit', 'f1ab', 'Language Settings', 'Sprog Indstillinger', 'Y', 'N');
      INSERT INTO cesu_uxsc_sc(pk1, linkUrl, imageSource, caption_en, caption_da, display_ind, required_ind) VALUES (cesu_uxsc_sc_SEQ.nextval, '/webapps/blackboard/execute/nautilus/notificationSettingsCaret?action=display', 'f0f3', 'Notifications Settings', 'Indstillinger for Notifikationer', 'Y', 'Y');
  END IF;
END;
/

SELECT * FROM cesu_uxsc_sc
/