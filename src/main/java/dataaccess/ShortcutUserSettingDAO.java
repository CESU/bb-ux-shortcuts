package dataaccess;

import java.util.List;

import blackboard.persist.Id;
import blackboard.persist.dao.impl.SimpleDAO;
import blackboard.persist.impl.SimpleSelectQuery;
import models.ShortcutUserSetting;

public class ShortcutUserSettingDAO extends SimpleDAO<ShortcutUserSetting> {
	
	private static ShortcutUserSettingDAO instance;
	
	private ShortcutUserSettingDAO() {
		super(ShortcutUserSetting.class);
	}
	
	private ShortcutUserSettingDAO(Class<ShortcutUserSetting> cls) {
		super(cls);
	}
	
	public static ShortcutUserSettingDAO getInstance() {
		if (instance == null) {
			instance = new ShortcutUserSettingDAO();
		}
		
		return instance;
	}
	
	public List<ShortcutUserSetting> GetShortcutSettings(Id userId) {
		SimpleSelectQuery query = new SimpleSelectQuery(this.getDAOSupport().getMap());
		query.addWhere("userId", userId);

		/*return query.getResults();*/
		return getDAOSupport().loadList(query);
	}
	
	public void save(ShortcutUserSetting setting) {
		getDAOSupport().persist(setting);
	}
}