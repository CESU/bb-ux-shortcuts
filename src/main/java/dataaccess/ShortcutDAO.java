package dataaccess;

import java.util.Comparator;
import java.util.List;

import blackboard.persist.dao.impl.SimpleDAO;
import blackboard.persist.impl.SimpleSelectQuery;
import models.Shortcut;

public class ShortcutDAO extends SimpleDAO<Shortcut> {
	
	private static ShortcutDAO instance;
	
	private ShortcutDAO() {
		super(Shortcut.class);
	}
	
	private ShortcutDAO(Class<Shortcut> cls) {
		super(cls);
	}
	
	public static ShortcutDAO getInstance() {
		if (instance == null) {
			instance = new ShortcutDAO();
		}
		
		return instance;
	}
	
	public List<Shortcut> GetShortcuts(String language) {
		SimpleSelectQuery query = new SimpleSelectQuery(this.getDAOSupport().getMap());

		List<Shortcut> shortcuts =  getDAOSupport().loadList(query);
		
		switch (language) {
			case "da":
				shortcuts.sort(Comparator.comparing(Shortcut::getCaptionDa));
				break;
	
			case "en":
				shortcuts.sort(Comparator.comparing(Shortcut::getCaptionEn));
				break;
		}
		
		/*return query.getResults();*/
		return shortcuts;
	}
	
	public void save(Shortcut shortcut) {
		getDAOSupport().persist(shortcut);
	}
}
