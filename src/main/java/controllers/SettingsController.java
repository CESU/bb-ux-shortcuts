package controllers;


import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import blackboard.data.ReceiptOptions;
import blackboard.platform.plugin.PlugInUtil;
import blackboard.platform.servlet.InlineReceiptUtil;
import dataaccess.ShortcutDAO;
import models.Shortcut;


/**
 * Servlet implementation class LandingPageController
 */
@WebServlet("/settings")
public class SettingsController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SettingsController() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// verify associated with course
		try {
			if ( !PlugInUtil.authorizeForSystemAdmin(request, response) ) {
				return;
			}
		} catch (Exception e) {
			throw new RuntimeException(e);
		}

		// init 
		ReceiptOptions msg = new ReceiptOptions();
		
		// Gets the user's language setting
		String language = EditController.GetUserLanguage();
		
		// gets the shorcuts to display settings for
		List<Shortcut> shortcuts = ShortcutDAO.getInstance().GetShortcuts(language);
		
		// sets the list of shortcuts to be displayed in the view
		request.setAttribute("scList", shortcuts);
		
		// set cancel url in context
		request.setAttribute("recallUrl", "/webapps/plugins/execute/plugInController");
		
		// sets the language to be used for localization
		request.setAttribute("language", language);
		
		// set the view
		String view = InlineReceiptUtil.addReceiptToUrl("/WEB-INF/jsp/settings.jsp", msg);
		
		// forward the user to the view
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(view);
		requestDispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// verify associated with course
		try {
			if ( !PlugInUtil.authorizeForSystemAdmin(request, response) ) {
				return;
			}
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		
		// init inline message
		ReceiptOptions msg = new ReceiptOptions();
		
		// Gets the user's language setting
		String language = EditController.GetUserLanguage();
		
		// gets the shorcuts to change settings for
		List<Shortcut> shortcuts = ShortcutDAO.getInstance().GetShortcuts(language);
						
		// update 
		try {
			for (Shortcut sc : shortcuts) {
				sc.setDisplayed( Boolean.parseBoolean(request.getParameter(String.format("dis_lcbe%s", sc.getId().getExternalString()))) );
				sc.setRequired( Boolean.parseBoolean(request.getParameter(String.format("req_lcbe%s", sc.getId().getExternalString()))) );
								
				ShortcutDAO.getInstance().save(sc);
			}
			
			msg.addSuccessMessage("Your settings have been saved.");
		} catch (Exception e) {
			msg.addErrorMessage("Error while saving settings. " + e.getMessage(), e);
		}
		
		// set the view
		String view =  InlineReceiptUtil.addReceiptToUrl("/webapps/plugins/execute/plugInController", msg);

		// send the user to the "students"-page
		response.sendRedirect(view);
	}
}