package controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.ResourceBundle;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import blackboard.data.ReceiptOptions;
import blackboard.persist.Id;
import blackboard.platform.context.Context;
import blackboard.platform.context.ContextManagerFactory;
import blackboard.platform.intl.BbLocale;
import blackboard.platform.intl.LocaleManagerFactory;
import blackboard.platform.plugin.PlugInUtil;
import blackboard.platform.servlet.InlineReceiptUtil;
import blackboard.portal.data.Module;
import blackboard.portal.servlet.PortalUtil;
import models.Shortcut;
import models.ShortcutUserSetting;
import dataaccess.ShortcutDAO;
import dataaccess.ShortcutUserSettingDAO;

/**
 * Servlet implementation class LandingPageController
 */
@WebServlet("/module/view")
public class ModuleViewController extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ModuleViewController() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// verify associated with course
		try {
			if ( !PlugInUtil.ensureAuthenticatedUser(request, response) ) {
				return;
			}
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		
		// init 
		ReceiptOptions msg = new ReceiptOptions();
		
		// get the user and course from context
		Context context = ContextManagerFactory.getInstance().setContext(request);
		Id userId = context.getUserId();
		
		// Gets the user's language setting
		String language = EditController.GetUserLanguage();
		
		// gets the shorcuts to display
		List<Shortcut> shortcuts = ShortcutDAO.getInstance().GetShortcuts(language);
		
		// filter on display settings		
		shortcuts = FilterDisplayedShortcuts(shortcuts, userId);
		
		// Shows alternative message if there are no items to display
		if (shortcuts.isEmpty()) {
			Shortcut sc = new Shortcut();
						
			// Localization
	        Locale currentLocale = new Locale(language);
	        ResourceBundle rb = ResourceBundle.getBundle("text", currentLocale);
			
	        //TODO: Is this necessary?
			switch (language) {
				case "da":
					sc.setCaptionDa(rb.getString("shortcut.empty.caption"));
					break;
				case "en":
					sc.setCaptionEn(rb.getString("shortcut.empty.caption"));
					break;
			}
			
			// Create the settings url for the module
			Module module = PortalUtil.getModule(request);
			String tab_tab_group_id = context.getTabTabGroupId().toExternalString();
			String moduleId = module.getId().getExternalString();
			
			String url = String.format("?tab_tab_group_id=%s&forwardUrl=edit_module/%s/edit&recallUrl=?tab_tab_group_id=%s", tab_tab_group_id, moduleId, tab_tab_group_id);
			
			sc.setImageSource("");
			sc.setLinkUrl(url);
			
			shortcuts.add(sc);
		}
				
		// Get a list of avaliable languages
		List<BbLocale> locales = LocaleManagerFactory.getInstance().getAll();
		List<BbLocale> avaliableLocales = new ArrayList<>();

		for (BbLocale bbLocale : locales) {
			
			if (bbLocale.getIsAvailable()) {
				avaliableLocales.add(bbLocale);
			}
		}
		
		// sets the list of language options to be displayed
		request.setAttribute("languages", avaliableLocales);
						
		// sets the list of shortcuts to be displayed in the view
		request.setAttribute("scList", shortcuts);
		
		// sets the language to be used for localization
		request.setAttribute("language", language);
		
		// sets the current user local
		request.setAttribute("userLocal", LocaleManagerFactory.getInstance().getLocale().getLocaleObject().toString());
		
		// set the view
		String view = InlineReceiptUtil.addReceiptToUrl("/WEB-INF/jsp/module/view.jsp", msg);
		
		// forward the user to the view
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(view);
		requestDispatcher.forward(request, response);		
	}
		
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// verify associated with course
		try {
			if ( !PlugInUtil.ensureAuthenticatedUser(request, response) ) {
				return;
			}
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		
		// init inline message
		ReceiptOptions msg = new ReceiptOptions();
				
		// get the current context
		Context context = ContextManagerFactory.getInstance().setContext(request);
				
		// if language change is initiated, get the value here
		String newLocale = request.getParameter("langSelect");
		
		if (newLocale != null && !newLocale.isEmpty()) {
			context.getUser().setLocale(newLocale);
			doGet(request, response);
		}
		
		String view = InlineReceiptUtil.addReceiptToUrl("/WEB-INF/jsp/module/view.jsp", msg);
		//String view = InlineReceiptUtil.addReceiptToUrl("http://localhost:9876/webapps/portal/execute/tabs/tabAction?tab_tab_group_id=_1_1", msg);

		// send the user to the "students"-page
		response.sendRedirect(view);
	}	
	
	/** 
	 * Checks the admin and user settings, and if none indicate that the shortcut should be displayed, removes it from the list
	 * */
	private List<Shortcut> FilterDisplayedShortcuts(List<Shortcut> shortcuts, Id userId) {
		
		List<Shortcut> filteredShortcuts = new ArrayList<>(shortcuts);		
		List<ShortcutUserSetting> settings = ShortcutUserSettingDAO.getInstance().GetShortcutSettings(userId);
		
		for (Shortcut sc : shortcuts) {			
			// Check  display settings
			Optional<ShortcutUserSetting> opt = settings.stream()
					.filter(x -> x.getShortcutId().equals(sc.getId()))
					.findFirst();
				
			ShortcutUserSetting setting = opt.orElse(null);
			
			Boolean settingDisplay = null;
			
			if (setting != null) {
				settingDisplay = setting.isDisplayed();
			}
			
			if (sc.CalcDisplay(settingDisplay))
				continue;
			
			filteredShortcuts.remove(sc);
		}
		
		return filteredShortcuts;
	}
}
