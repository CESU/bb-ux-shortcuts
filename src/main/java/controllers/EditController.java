package controllers;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import blackboard.data.ReceiptOptions;
import blackboard.persist.Id;
import blackboard.platform.context.Context;
import blackboard.platform.context.ContextManagerFactory;
import blackboard.platform.intl.LocaleManagerFactory;
import blackboard.platform.plugin.PlugInUtil;
import blackboard.platform.servlet.InlineReceiptUtil;

import models.Shortcut;
import models.ShortcutUserSetting;
import dataaccess.ShortcutDAO;
import dataaccess.ShortcutUserSettingDAO;

/**
 * Servlet implementation class LandingPageController
 */
@WebServlet("/module/edit")
public class EditController extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EditController() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// verify associated with course
		try {
			if ( !PlugInUtil.ensureAuthenticatedUser(request, response) ) {
				return;
			}
		} catch (Exception e) {
			throw new RuntimeException(e);
		}

		// init 
		ReceiptOptions msg = new ReceiptOptions();
		
		// get the user and course from context
		Context context = ContextManagerFactory.getInstance().setContext(request);
		Id userId = context.getUserId();
		
		// Gets the user's language setting
		String language = GetUserLanguage();
				
		// gets the shorcuts to display settings for
		List<Shortcut> shortcuts = ShortcutDAO.getInstance().GetShortcuts(language);

		// gets the personalized settings for the current user
		List<ShortcutUserSetting> scSettings = ShortcutUserSettingDAO.getInstance().GetShortcutSettings(userId);
		
		for (Shortcut sc : shortcuts) {			
			// Check  display settings
			Optional<ShortcutUserSetting> opt = scSettings.stream()
					.filter(x -> x.getShortcutId().equals(sc.getId()))
					.findFirst();
				
			ShortcutUserSetting setting = opt.orElse(null);
			
			Boolean settingDisplay = null;
			
			if (setting != null) {
				settingDisplay = setting.isDisplayed();
			}
			
			sc.setDisplayedCurrentUser(sc.CalcDisplay(settingDisplay));
		}
		
		// sets the list of shortcuts to be displayed in the view
		request.setAttribute("scList", shortcuts);		
		
		// sets the Url to follow after cancel
		request.setAttribute("recallUrl", request.getParameter("recallUrl"));
		
		// sets the language to be used for localization
		request.setAttribute("language", language);

		// set the view
		String view = InlineReceiptUtil.addReceiptToUrl("/WEB-INF/jsp/module/edit.jsp", msg);
		
		// forward the user to the view
		RequestDispatcher requestDispatcher = getServletContext().getRequestDispatcher(view);
		requestDispatcher.forward(request, response);
	}
	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// verify associated with course
		try {
			if ( !PlugInUtil.ensureAuthenticatedUser(request, response) ) {
				return;
			}
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		
		// init inline message
		ReceiptOptions msg = new ReceiptOptions();
		
		// get parameters
		//List<Shortcut> shortcuts = request.getParameter("");
		
		// get the user and course from context
		Context context = ContextManagerFactory.getInstance().setContext(request);
		Id userId = context.getUserId();
		
		// Gets the user's language setting
		String language = GetUserLanguage();
		
		// gets the shorcuts to change settings for
		List<Shortcut> shortcuts = ShortcutDAO.getInstance().GetShortcuts(language);
		
		// gets the personalized settings for the current user
		List<ShortcutUserSetting> scSettings = ShortcutUserSettingDAO.getInstance().GetShortcutSettings(userId);
				
		// update 
		try {
			for (Shortcut sc : shortcuts) {
				Optional<ShortcutUserSetting> opt = scSettings.stream()
					.filter(x -> x.getShortcutId().equals(sc.getId()))
					.findFirst();
				
				ShortcutUserSetting setting = opt.orElse(new ShortcutUserSetting(userId, sc.getId()));
							
				setting.setDisplayed( Boolean.parseBoolean(request.getParameter(String.format("hidden%s", sc.getId().getExternalString()))) );
								
				ShortcutUserSettingDAO.getInstance().save(setting);
			}
			
			msg.addSuccessMessage("Your settings have been saved.");
		} catch (Exception e) {
			msg.addErrorMessage("Error while saving settings. " + e.getMessage(), e);
		}
		
		String view = request.getParameter("recallUrl");

		// send the user to the "students"-page
		response.sendRedirect(InlineReceiptUtil.addReceiptToUrl(view, msg));
	}
	
	/**
	 * Gets the user chosen language, and formats it to [ll]
	 * 
	 * @return The formatted language string
	 */
	public static String GetUserLanguage() {
		
		String locale = LocaleManagerFactory.getInstance().getLocale().getLocaleObject().toString();
		
		if (locale != null && !locale.isEmpty()) {
			switch (locale) {
				case "en_GB":
				case "en_US":
					return "en";
		
				case "da_DK":
					return "da";
					
				default:
					return "da";
			}	
		}
		
		return "da";		
	}
}