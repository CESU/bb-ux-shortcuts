package models;

import blackboard.data.AbstractIdentifiable;
import blackboard.data.user.User;
import blackboard.persist.DataType;
import blackboard.persist.Id;
import blackboard.persist.impl.mapping.annotation.Column;
import blackboard.persist.impl.mapping.annotation.PrimaryKey;
import blackboard.persist.impl.mapping.annotation.RefersTo;
import blackboard.persist.impl.mapping.annotation.Table;

@Table("cesu_uxsc_sc_settings")
public class ShortcutUserSetting extends AbstractIdentifiable {
	public static final DataType DATA_TYPE = new DataType(ShortcutUserSetting.class);
	@Override
	public DataType getDataType() { return DATA_TYPE; }
	
	@PrimaryKey
	private int pk;
	
	@Column(value = "user_id")
	@RefersTo(User.class)
	private Id userId;
	
	@Column(value = "sc_id")
	@RefersTo(Shortcut.class)
	private Id shortcutId;
	
	@Column(value = "display_ind")
	private String isDisplayed;
	
	/**
	 * Constructor for use of the DAO system only
	 * */
	public ShortcutUserSetting() {
	}
	
	public ShortcutUserSetting(Id userId, Id shortcutId) {
		this.userId = userId;
		this.shortcutId = shortcutId;
	}
	
	/**
	 * @return the userId
	 */
	public Id getUserId() {
		return userId;
	}
	
	/**
	 * @param userId the userId to set
	 */
	public void setUserId(Id userId) {
		this.userId = userId;
	}
	
	/**
	 * @return the shortcutId
	 */
	public Id getShortcutId() {
		return shortcutId;
	}
	
	/**
	 * @param shortcutId the shortcutId to set
	 */
	public void setShortcutId(Id shortcutId) {
		this.shortcutId = shortcutId;
	}
	
	/**
	 * @return the isDisplayed
	 */
	public boolean isDisplayed() {
		
		boolean result = false;
		
		if(isDisplayed != null && !isDisplayed.isEmpty() ) {

			result = isDisplayed.equals("Y");
		}
		
		return result;
	}

	/**
	 * @param isDisplayed the isDisplayed to set
	 */
	public void setDisplayed(boolean input) {
		
		this.isDisplayed = input ? "Y" : "N";
	}
}
