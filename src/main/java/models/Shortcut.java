package models;

import blackboard.data.AbstractIdentifiable;
import blackboard.persist.DataType;
import blackboard.persist.impl.mapping.annotation.Column;
import blackboard.persist.impl.mapping.annotation.PrimaryKey;
import blackboard.persist.impl.mapping.annotation.Table;

@Table("cesu_uxsc_sc")
public class Shortcut extends AbstractIdentifiable {
	public static final DataType DATA_TYPE = new DataType(Shortcut.class);
	@Override
	public DataType getDataType() { return DATA_TYPE; }
	
	@PrimaryKey
	private int pk;
	
	@Column(value = "linkUrl")
	private String linkUrl;
	
	@Column(value = "imageSource")
	private String imageSource;
	
	@Column(value = "caption_en")
	private String captionEn;
	
	@Column(value = "caption_da")
	private String captionDa;
	
	@Column(value = "display_ind")
	private String isDisplayed;
	
	@Column(value = "required_ind")
	private String isRequired;
	
	private boolean isDisplayedCurrentUser;
	
	public String getLinkUrl() {
		return linkUrl;
	}

	public void setLinkUrl(String href) {
		this.linkUrl = href;
	}

	public String getImageSource() {
		return imageSource;
	}

	public void setImageSource(String src) {
		this.imageSource = src;
	}

	public String getCaptionEn() {
		return captionEn;
	}

	public void setCaptionEn(String caption) {
		this.captionEn = caption;
	}
	
	public String getCaptionDa() {
		return captionDa;
	}

	public void setCaptionDa(String caption) {
		this.captionDa = caption;
	}

	public boolean isDisplayed() {
		
		boolean result = false;
		
		if(isDisplayed != null && !isDisplayed.isEmpty() ) {

			result = isDisplayed.equals("Y");
		}
		
		return result;
	}

	public void setDisplayed(boolean input) {
		
		this.isDisplayed = input ? "Y" : "N";
	}

	/**
	 * @return the isRequired
	 */
	public boolean isRequired() {
		
		boolean result = false;
		
		if(isRequired != null && !isRequired.isEmpty() ) {

			result = isRequired.equals("Y");
		}
		
		return result;
	}

	/**
	 * @param isRequired the isRequired to set
	 */
	public void setRequired(boolean input) {
				
		this.isRequired = input ? "Y" : "N";
	}

	/**
	 * @return the isDisplayedCurrentUser
	 */
	public boolean isDisplayedCurrentUser() {
		return isDisplayedCurrentUser;
	}

	/**
	 * @param isDisplayedCurrentUser the isDisplayedCurrentUser to set
	 */
	public void setDisplayedCurrentUser(boolean isDisplayedCurrentUser) {
		this.isDisplayedCurrentUser = isDisplayedCurrentUser;
	}
	
	public boolean CalcDisplay(Boolean userDisplaySetting) {
		boolean result = true;
		
		// Check admin required
		if (!isRequired()) {
			
			// Check user display
			if (userDisplaySetting != null) {
				result = userDisplaySetting;
			}
			// Check admin display
			else {
				result = isDisplayed();
			}
		}
		
		return result;
	}
}
